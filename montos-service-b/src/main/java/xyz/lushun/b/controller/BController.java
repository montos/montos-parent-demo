package xyz.lushun.b.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.lushun.b.response.BaseResponse;

/**
 * @author Montos
 * @create 2021/2/10 10:51 上午
 */
@RestController
@RequestMapping("/api/b/")
public class BController {

    @RequestMapping("hello/v1")
    public BaseResponse<String> getHello(){
        BaseResponse<String> objectBaseResponse = new BaseResponse<String>();
        objectBaseResponse.setData("i am b");
        return objectBaseResponse;
    }
}
