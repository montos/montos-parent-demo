package xyz.lushun.b.service.impl;

import org.springframework.stereotype.Service;
import xyz.lushun.b.service.IServiceB;

/**
 * @author Montos
 * @create 2021/2/10 11:11 上午
 */
@Service
public class IServiceBImpl implements IServiceB {


    @Override
    public String sayHello(String name) {
        return name + " b service";
    }
}
