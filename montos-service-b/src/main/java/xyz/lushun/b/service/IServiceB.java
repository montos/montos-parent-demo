package xyz.lushun.b.service;

/**
 * @author Montos
 * @create 2021/2/10 10:59 上午
 */
public interface IServiceB {

    String sayHello(String name);

}
