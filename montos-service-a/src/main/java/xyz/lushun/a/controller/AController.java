package xyz.lushun.a.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.lushun.a.feignService.b.IServiceB;
import xyz.lushun.a.response.b.BaseResponse;

/**
 * @author Montos
 * @create 2021/2/10 11:12 上午
 */
@AllArgsConstructor
@RestController
@RequestMapping("/api/a")
public class AController {

    private final IServiceB iServiceB;


    @RequestMapping("/sayHello/v1")
    public String sayHello(){
        BaseResponse<String> hello = iServiceB.getHello();
        return "a"+ hello.getData();
    }
}
