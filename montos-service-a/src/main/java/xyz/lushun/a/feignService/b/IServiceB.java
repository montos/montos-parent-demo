package xyz.lushun.a.feignService.b;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import xyz.lushun.a.response.b.BaseResponse;

/**
 * @author Montos
 * @create 2021/2/10 10:49 上午
 */
@FeignClient(name = "serviceB")
public interface IServiceB {

     @RequestMapping("/api/b/hello/v1")
     BaseResponse<String> getHello();
}
