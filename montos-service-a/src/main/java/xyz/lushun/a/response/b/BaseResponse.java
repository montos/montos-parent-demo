package xyz.lushun.a.response.b;

import lombok.Data;

/**
 * @author Montos
 * @create 2021/2/10 10:53 上午
 */
@Data
public class BaseResponse<T> {

    private int code = 200;

    private T data;

    private String message;

}
